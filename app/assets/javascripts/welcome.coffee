$ ->
  app = angular.module 'cloudGlaze', []

  app.directive 'pressEnter', ->
    (scope, element, attrs) ->
      element.bind 'keypress', (event) ->
        if event.which == 13
          scope.$apply scope.goSearch()
          event.preventDefault()

  app.controller 'crawlersController', ($scope, $http) ->
    $http.post "/crawler/brands_list",
      crawler_name: 'gsmarena'
    .success (response, status) ->
      $scope.brands = response.result
    .error (response, status) ->
      console.error "Oops, Error: #{JSON.stringify(response)}"

    $scope.goSearch = ->
      $http.post "/crawler/search_products",
        crawler_name: 'gsmarena'
        keyword:       $scope.keyword
      .success (response, status) ->
        $scope.prod_items = response.result
        $('form#search_form').removeClass('ng-submitted')
      .error (response, status) ->
        console.error "Oops, Error: #{JSON.stringify(response)}"

    $scope.$watch 'brand_link', (newValue, oldValue, scope) ->
      if newValue?
        $http.post "/crawler/select_brand",
          crawler_name: 'gsmarena'
          brand:        newValue
        .success (response, status) ->
          $scope.products = response.result
        .error (response, status) ->
          console.error "Oops, Error: #{JSON.stringify(response)}"

    $scope.$watch 'product_link', (newValue, oldValue, scope) ->
      if newValue?
        $http.post "/crawler/show_product",
          crawler_name: 'gsmarena'
          product:      newValue
        .success (response, status) ->
          $scope.product_info = JSON.parse(response.result)
        .error (response, status) ->
          console.error "Oops, Error: #{JSON.stringify(response)}"
