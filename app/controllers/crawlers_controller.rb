class CrawlersController < ApplicationController
  before_action :load_crawler

  def brands_list
    brands = @crawler.dig(:brands_list)
    render json: {result: brands}
  end

  def select_brand
    products = @crawler.dig(:select_brand, params[:brand])
    render json: {result: products}
  end

  def show_product
    product = @crawler.dig(:show_product, params[:product])
    render json: {result: product}
  end

  def search_products
    products = @crawler.dig(:search_products, params[:keyword])
    render json: {result: products}
  end

  private
  def load_crawler
    @crawler = crawlers[params[:crawler_name]]
  end
end
