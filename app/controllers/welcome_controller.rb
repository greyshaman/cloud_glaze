class WelcomeController < ApplicationController
  def index
    @crawlers = crawlers
    default_crawler = @crawlers&.values&.first
    @brands = default_crawler.dig(:brands_list)

    respond_to do |format|
      format.html
    end
  end
end
