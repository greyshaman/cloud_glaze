class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  helper_method :crawlers

  def crawlers
    CrawlersWorkshop.crawlers
  end
end
