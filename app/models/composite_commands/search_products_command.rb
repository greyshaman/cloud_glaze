class CompositeCommands::SearchProductsCommand < CompositeCommands::CompositeCommand

  def initialize(settings = {})
    super(settings)

    add_command Commands::SendRequestCommand.new(@settings)
    add_command Commands::ParseProductsResultCommand.new(@settings)
  end
end
