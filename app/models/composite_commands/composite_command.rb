class CompositeCommands::CompositeCommand
  def initialize(settings = {})
    @commands = []
    @settings = settings
  end

  def add_command(command)
    @commands << command
  end

  def execute(precondition = nil)
    chain_result = precondition
    @commands.each { |command| chain_result = command.execute(chain_result) }
    chain_result
  end
end
