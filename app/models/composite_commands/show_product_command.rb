class CompositeCommands::ShowProductCommand < CompositeCommands::CompositeCommand

  def initialize(settings = {})
    super(settings)

    add_command Commands::SendRequestCommand.new(@settings)
    add_command Commands::ParseProductInfoCommand.new(@settings)
  end
end
