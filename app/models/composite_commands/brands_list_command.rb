class CompositeCommands::BrandsListCommand < CompositeCommands::CompositeCommand

  def initialize(settings = {})
    super(settings)

    add_command Commands::SendRequestCommand.new(@settings)
    add_command Commands::GetOptionsListCommand.new(@settings)
  end
end
