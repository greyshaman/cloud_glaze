module Serializable
  extend ActiveSupport::Concern

  def attributes
    instance_values
  end

  def attributes=(hash)
    hash.each do |key, value|
      send("#{key}=", value) if self.respond_to? "#{key}="
    end
  end
end
