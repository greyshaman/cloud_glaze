class ProductSpecs::Platform
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'OS'        => 'os',
    'Chipset'   => 'chipset',
    'CPU'       => 'cpu',
    'GPU'       => 'gpu',
    ' '         => 'other'
  }

  attr_accessor :os, :chipset, :cpu, :gpu, :other
end
