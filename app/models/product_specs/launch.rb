class ProductSpecs::Launch
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Announced' => 'announced',
    'Status'    => 'status',
    ' '         => 'other'
  }

  attr_accessor :announced, :status, :other
end
