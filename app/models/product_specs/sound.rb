class ProductSpecs::Sound
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Alert type'  => 'alert_types',
    'Loudspeaker' => 'loudspeaker',
    '3.5mm jack'  => 'jack_35mm',
    ' '           => 'other'
  }

  attr_accessor :alert_types, :loudspeaker, :jack_35mm, :other
end
