class ProductSpecs::Battery
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    ' '         => 'technology',
    'Stand-by'  => 'stand_by',
    'Talk time' => 'talk_time'
  }

  attr_accessor :technology, :stand_by, :talk_time
end
