class ProductSpecs::Body
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Dimensions'  => 'dimensions',
    'Weight'      => 'weight',
    'SIM'         => 'sim',
    ' '           => 'other'
  }

  attr_accessor :dimensions, :weight, :sim, :other
end
