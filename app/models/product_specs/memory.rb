class ProductSpecs::Memory
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Card slot' => 'card_slot',
    'Internal'  => 'internal',
    ' '         => 'other'
  }

  attr_accessor :card_slot, :internal, :other
end
