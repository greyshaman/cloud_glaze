class ProductSpecs::Comms
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'WLAN'      => 'wlan',
    'Bluetooth' => 'bluetooth',
    'GPS'       => 'gps',
    'Radio'     => 'radio',
    'USB'       => 'usb',
    ' '         => 'other'
  }

  attr_accessor :wlan, :bluetooth, :gps, :radio, :usb, :other
end
