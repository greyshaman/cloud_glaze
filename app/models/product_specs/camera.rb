class ProductSpecs::Camera
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Primary'   => 'primary',
    'Features'  => 'features',
    'Video'     => 'video',
    'Secondary' => 'secondary',
    ' '         => 'other'
  }

  attr_accessor :primary, :features, :video, :secondary, :other
end
