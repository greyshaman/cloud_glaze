class ProductSpecs::Features
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Sensors'   => 'sensors',
    'Messaging' => 'messaging',
    'Browser'   => 'browser',
    'Java'      => 'java',
    ' '         => 'other'
  }

  attr_accessor :sensors, :messaging, :browser, :java, :other
end
