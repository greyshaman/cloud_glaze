class ProductSpecs::Network
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Technology'  => 'technology',
    '2G bands'    => 'bands_2g',
    '3G bands'    => 'bands_3g',
    '4G bands'    => 'bands_4g',
    'Speed'       => 'speed',
    'GPRS'        => 'gprs',
    'EDGE'        => 'edge',
    ' '           => 'other'
  }

  attr_accessor :technology, :bands_2g, :bands_3g, :bands_4g, :speed, :gprs, :edge, :other
end
