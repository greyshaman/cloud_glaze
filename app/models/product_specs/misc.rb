class ProductSpecs::Misc
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Colors'      => 'colors',
    'SAR US'      => 'sar_us',
    'SAR EU'      => 'sar_eu',
    'Price group' => 'price_group',
    ' '           => 'other'
  }

  attr_accessor :colors, :sar_us, :sar_eu, :price_group, :other
end
