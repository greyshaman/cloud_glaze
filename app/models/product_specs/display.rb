class ProductSpecs::Display
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Type'        => 'type',
    'Size'        => 'size',
    'Resolution'  => 'resolution',
    'Multitouch'  => 'multitouch',
    ' '           => 'other'
  }

  attr_accessor :type, :size, :resolution, :multitouch, :other
end
