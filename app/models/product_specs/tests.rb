class ProductSpecs::Tests
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON
  include Serializable

  ATTR_IMPORT_MAP = {
    'Performance'     => 'performance',
    'Display'         => 'display',
    'Camera'          => 'camera',
    'Loudspeaker'     => 'loudspeaker',
    'Audio quality'   => 'audio_quality',
    'Battery life'    => 'battery_life',
    ' '               => 'other'
  }

  attr_accessor :performance, :display, :camera, :loudspeaker, :audio_quality, :battery_life, :other
end
