class Commands::Command
  TARGET_PATTERN = ':target'

  def initialize(settings = {})
    @settings = settings
  end

  # simple chain execution
  def execute(precondition = nil)
    precondition
  end
end
