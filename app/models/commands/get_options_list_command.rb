class Commands::GetOptionsListCommand < Commands::Command
  def execute(page = nil)
    result = {}
    if page && page.instance_of?(Nokogiri::HTML::Document)
      result = page.css(@settings[:source_selector]).each_with_object({}) { |element, hh| hh[element.text] = element['href']}
    end
    result
  end
end
