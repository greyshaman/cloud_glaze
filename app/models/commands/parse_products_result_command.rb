class Commands::ParseProductsResultCommand < Commands::Command
  def execute(page = nil)
    result = []
    if page && page.instance_of?(Nokogiri::HTML::Document)
      result = page.css(@settings[:source_selector]).map { |element| element.text }
    end
    result
  end
end
