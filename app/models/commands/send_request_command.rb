class Commands::SendRequestCommand < Commands::Command
  def initialize(settings = {})
    super(settings)
    @target_url = @settings[:url].clone
    @target_url << @settings[:activation_path] if @settings[:activation_path]
  end

  def execute(target = nil)
    @target_url.gsub!(':target', target) if target
    Nokogiri::HTML(open(@target_url))
  end
end
