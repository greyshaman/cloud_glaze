class Commands::ParseProductInfoCommand < Commands::Command
  ATTR_CLASS_PREFIX = 'ProductSpecs'.freeze

  def execute(page = nil)
    result = {}
    if page && page.instance_of?(Nokogiri::HTML::Document)
      product_model = page.css(@settings[:product_name_sel])&.text
      product       = Product.new model: product_model
      spec_items    = page.css(@settings[:spec_item_sel])
      spec_items.each do |spec_item|
        spec_name  = spec_item.css(@settings[:spec_name_sel])&.text
        spec_class = "#{ATTR_CLASS_PREFIX}::#{spec_name}".constantize
        spec       = spec_class.new
        spec_attrs = spec_item.css(@settings[:spec_attr_sel])

        spec_attrs.each_with_index do |attr_raw_data, index|
          begin
            attr_name  = spec_class.const_get(:ATTR_IMPORT_MAP)[attr_raw_data.css(@settings[:spec_attr_name]).text]
            attr_value = lookup_element(attr_raw_data, @settings[:spec_attr_value])&.text

            spec.send("#{attr_name}=", attr_value) if attr_name

          rescue NameError => ex
            Rails.logger.error "Cannot create ProductSpec with Type #{spec_name}: #{ex.message}"
            Rails.logger.error ex.backtrace.join("\n")
          rescue Exception => ex
            Rails.logger.error "Cannot parse attribute with name #{attr_name} for spec with Type #{spec_name}: #{ex.message}"
            Rails.logger.error ex.backtrace.join("\n")
          end
        end

        product.send("#{spec_name.downcase}=", spec) if spec && product.respond_to?("#{spec_name.downcase}=")
      end
      result = product
    end
    result.to_json
  end

  private
  def lookup_element(container, selectors)
    found_element = nil
    selectors     = [selectors] unless selectors.is_a? Array

    selectors.each do |selector|
      found_element = container.css(selector)
      break if found_element
    end
    found_element
  end
end
