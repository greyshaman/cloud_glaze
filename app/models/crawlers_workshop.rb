class CrawlersWorkshop
  attr_accessor :crawlers

  def self.crawlers(crawlers_config = Rails.application.config.crawlers)
    crawlers = {}
    crawlers_config.each do |crawler_name, crawler_options|
      crawler_options.deep_symbolize_keys!
      crawlers.merge!(crawler_name.to_s => Crawler.new(crawler_name, crawler_options[:url], crawler_options[:instructions]))
    end
    crawlers
  end
end
