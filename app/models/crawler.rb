class Crawler
  include ActiveModel::Model

  COMPOSITE_COMMAND_CLASS_PREFIX = 'CompositeCommands'
  COMMAND_CLASS_SUFFIX           = 'Command'

  attr_accessor :name, :url, :instructions

  def initialize(name, url = '', instructions = {})
    @name         = name
    @url          = url
    @instructions = {}
    instructions.each_pair do |instr_name, instr_params|
      command_type = "#{COMPOSITE_COMMAND_CLASS_PREFIX}::#{instr_name.to_s.camelize}#{COMMAND_CLASS_SUFFIX}"
      @instructions.merge!(instr_name => command_type.constantize.new({url: url}.reverse_merge(instr_params)))
    end
  end

  def dig(instruction_name, target = nil)
    result = @instructions[instruction_name] && @instructions[instruction_name].execute(target)
  end
end
