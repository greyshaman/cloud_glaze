class Product
  include ActiveModel::Model
  include ActiveModel::Serializers::JSON

  PRODUCT_SPECS = %i(network launch body display platform memory camera sound comms features battery misc tests).freeze

  attr_accessor :model, :specs


  def initialize(opts = {})
    @model = opts[:model] || nil
    @specs = opts[:specs] || {}
  end

  def attributes
    {
      model: @model,
      specs: @specs
    }
  end

  def attributes=(hash)
    hash.each do |key, value|
      case key
      when :model
        @model = value
      when :specs
        @specs = value
      else
        @specs.merge!(key => value)
      end
    end
  end

  PRODUCT_SPECS.each do |spec|
    define_method spec do
      @specs[spec]
    end

    define_method "#{spec}=" do |value|
      @specs[spec] = value
    end
  end
end
