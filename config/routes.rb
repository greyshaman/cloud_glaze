Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resource :crawler, only: [] do
    collection do
      post :brands_list
      post :select_brand
      post :show_product
      post :search_products
    end
  end

  root to: 'welcome#index'
end
