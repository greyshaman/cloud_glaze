FactoryGirl.define do
  factory :tests, class: ProductSpecs::Tests do
    performance     'Basemark OS II 2.0: 729 / Basemark X: 4072'
    display         'Contrast ratio: 940:1 (nominal), 2.254 (sunlight)'
    camera          'Photo / Video'
    loudspeaker     'Voice 74dB / Noise 66dB / Ring 72dB'
    audio_quality   'Noise -93.3dB / Crosstalk -93.6dB'
    battery_life    'Endurance rating 73h'
  end
end
