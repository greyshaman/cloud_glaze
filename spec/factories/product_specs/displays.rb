FactoryGirl.define do
  factory :display, class: ProductSpecs::Display do
    type        'IPS LCD capacitive touchscreen, 16M colors'
    size        '5.5 inches (~69.3% screen-to-body ratio)'
    resolution  '1080 x 1920 pixels (~401 ppi pixel density)'
    multitouch  'Yes'
  end
end
