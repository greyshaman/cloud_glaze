FactoryGirl.define do
  factory :memory, class: ProductSpecs::Memory do
    card_slot   'microSD, up to 256 GB (dedicated slot)'
    internal    '16 GB, 3 GB RAM - A7010 a48 8/16/32 GB, 2 GB RAM - A7010'
  end
end
