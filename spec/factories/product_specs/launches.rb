FactoryGirl.define do
  factory :launch, class: ProductSpecs::Launch do
    announced   '2015, August'
    status      'Available. Released 2015, September'
  end
end
