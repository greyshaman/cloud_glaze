FactoryGirl.define do
  factory :sound, class: ProductSpecs::Sound do
    alert_types   'Vibration; MP3, WAV ringtones'
    loudspeaker   'Yes, with stereo speakers'
    jack_35mm     'Yes'
  end
end
