FactoryGirl.define do
  factory :features, class: ProductSpecs::Features do
    sensors     'Fingerprint, accelerometer, gyro, proximity, compass'
    messaging   'SMS(threaded view), MMS, Email, Push Mail, IM'
    browser     'HTML5'
    java        'No'
  end
end
