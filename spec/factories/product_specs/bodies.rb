FactoryGirl.define do
  factory :body, class: ProductSpecs::Body do
    dimensions    '155.7 x 77.3 x 8.9 mm (6.13 x 3.04 x 0.35 in)'
    weight        '175 g (6.17 oz)'
    sim           'Dual SIM (Nano-SIM, dual stand-by)'
  end
end
