FactoryGirl.define do
  factory :platform, class: ProductSpecs::Platform do
    os        'Android OS, v5.1.1 (Lollipop), upgradable to v6.0 (Marshmallow)'
    chipset   'Mediatek MT6753'
    cpu       'Octa-core 1.3 GHz Cortex-A53'
    gpu       'Mali-T720MP3'
  end
end
