FactoryGirl.define do
  factory :comms, class: ProductSpecs::Comms do
    wlan        'Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot'
    bluetooth   'v4.1'
    gps         'Yes, with A-GPS, GLONASS'
    radio       'No'
    usb         'v3.0, Type-C reversible connector'
  end
end
