FactoryGirl.define do
  factory :network, class: ProductSpecs::Network do
    technology    'GSM / HSPA / LTE'
    bands_2g      'GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2'
    bands_3g      'HSDPA 850 / 900 / 1900 / 2100'
    bands_4g      'LTE band 1(2100), 3(1800), 5(850), 7(2600), 8(900), 20(800) - A7010'
    speed         'HSPA, LTE Cat4 150/50 Mbps'
    gprs          'Yes'
    edge          'Yes'
  end
end
