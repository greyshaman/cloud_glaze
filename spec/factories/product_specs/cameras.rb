FactoryGirl.define do
  factory :camera, class: ProductSpecs::Camera do
    primary     '13 MP, f/2.2, autofocus, OIS, dual-LED flash'
    features    '1.12 µm pixel size, geo-tagging, touch focus, face detection, panorama'
    video       '1080p@30fps, 1080p@60fps'
    secondary   '8 MP, f/2.2, 1080p@30fps'
  end
end
