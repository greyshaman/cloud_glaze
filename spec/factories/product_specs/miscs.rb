FactoryGirl.define do
  factory :misc, class: ProductSpecs::Misc do
    colors        'White, Dark Gray'
    sar_us        '0.84 W/kg (head)'
    sar_eu        '0.84 W/kg (head) '
    price_group   '4/10'
  end
end
