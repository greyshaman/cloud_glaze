FactoryGirl.define do
  factory :battery, class: ProductSpecs::Battery do
    technology  'Non-removable Li-Po 4100 mAh battery'
    stand_by    'Up to 526 h (3G)'
    talk_time   'Up to 38 h (3G)'
  end
end
