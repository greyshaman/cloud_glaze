require 'rails_helper'

RSpec.describe CrawlersWorkshop do
  context 'when build from application config' do
    # let(:workshop) { CrawlersWorkshop.new }

    # it { expect(workshop).to be_instance_of CrawlersWorkshop }
    it { expect(CrawlersWorkshop.crawlers).to be_instance_of Hash }
    it { expect(CrawlersWorkshop.crawlers.length).to eq(1) }
  end
end
