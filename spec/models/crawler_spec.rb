require 'rails_helper'

RSpec.describe Crawler do
  context 'when use settings from application config' do
    let!(:crawler) { CrawlersWorkshop.crawlers.values.first }

    it { expect(crawler).to be_instance_of Crawler }
    it { expect(crawler.url).to be }
    it { expect(crawler.instructions).not_to be_empty }

    context 'get brands list' do
      it 'should receive barnds list data' do
        VCR.use_cassette('brands_list') do
          expect(crawler.dig(:brands_list)['Samsung']).to eq 'samsung-phones-9.php'
        end
      end
    end

    context 'get products list for samsung brand' do
      it 'should receive products list data' do
        VCR.use_cassette('select_brand') do
          expect(crawler.dig(:select_brand, 'samsung-phones-9.php')['Z2']).to eq 'samsung_z2-8288.php'
        end
      end
    end

    context 'show product info for Z2 samsung cellphone' do
      it 'should receive product info and create product instance wich should serialized into json' do
        VCR.use_cassette('show_product') do
          expect(crawler.dig(:show_product, 'samsung_z2-8288.php')).to match /Samsung Z2/
        end
      end
    end

    context 'search for product with model name contains Z1' do
      it 'should return list of found products' do
        VCR.use_cassette('search_z1_products') do
          expect(crawler.dig(:search_products, 'Z1')).to include 'SonyXperia Z1'
        end
      end
    end

    context 'search for unexisting name contains baramara' do
      it 'should return list of found products' do
        VCR.use_cassette('search_baramara_products') do
          expect(crawler.dig(:search_products, 'baramara')).to be_empty
        end
      end
    end
  end


end
