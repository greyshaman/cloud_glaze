require 'rails_helper'

RSpec.describe CrawlersController, type: :controller do
  context '.brands_list' do
    before do
      VCR.use_cassette('brands_list') do
        post :brands_list, params: {crawler_name: 'gsmarena'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']['Samsung']).to eq "samsung-phones-9.php" }
  end

  context '.select_brand' do
    before do
      VCR.use_cassette('select_brand') do
        post :select_brand, params: {crawler_name: 'gsmarena', brand: 'samsung-phones-9.php'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']['Z2']).to eq 'samsung_z2-8288.php' }
  end

  context '.show_product samsing z2' do
    before do
      VCR.use_cassette('show_product') do
        post :show_product, params: {crawler_name: 'gsmarena', product: 'samsung_z2-8288.php'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']).to match /Samsung Z2/ }
  end

  context '.show_product samsing Note 7' do
    before do
      VCR.use_cassette('show_product_sgn7') do
        post :show_product, params: {crawler_name: 'gsmarena', product: 'samsung_galaxy_note7_(usa)-8242.php'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']).to match /Samsung Galaxy Note7 \(USA\)/ }
  end

  context '.search_products for Z1' do
    before do
      VCR.use_cassette('search_z1_products') do
        post :search_products, params: {crawler_name: 'gsmarena', keyword: 'Z1'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']).to include 'SonyXperia Z1' }
  end

  context '.search_products for baramara' do
    before do
      VCR.use_cassette('search_baramara_products') do
        post :search_products, params: {crawler_name: 'gsmarena', keyword: 'baramara'}
      end
    end

    it { expect(JSON.parse(response.body)['result']).to be }
    it { expect(JSON.parse(response.body)['result']).to be_empty }
  end
end
