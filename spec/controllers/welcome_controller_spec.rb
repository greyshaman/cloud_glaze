require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  describe '.index' do
    before do
      VCR.use_cassette('brands_list') do
        get :index
      end
    end

    it { expect(assigns[:crawlers]).not_to be_empty  }
    it { expect(assigns[:brands]).not_to be_empty  }
    it { expect(assigns[:brands]['Samsung']).to eq 'samsung-phones-9.php'  }
  end
end
